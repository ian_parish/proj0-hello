# CIS322 F20 - PROJECT 0

Author: Ian Parish
Email: iparish@cs.uoregon.edu

---------------------------------

This is a trivial piece of software that reads a message from a configuration
file and prints it to the terminal.
